﻿using System;
using System.Collections.Generic;

namespace Utilities
{
    public static class Memoizer
    {
        public static Func<TReturn> Memoize<TReturn>(Func<TReturn> func)
        {
            object cache = null;
            return () =>
            {
                if (cache == null)
                    cache = func();

                return (TReturn)cache;
            };
        }

        public static Func<TSource, TReturn> Memoize<TSource, TReturn>(Func<TSource, TReturn> func)
        {
            var cache = new Dictionary<TSource, TReturn>();
            return x =>
            {
                if (!cache.ContainsKey(x))
                    cache[x] = func(x);

                return cache[x];
            };
        }
    }
}
