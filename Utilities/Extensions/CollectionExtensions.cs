﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utilities.Extensions
{
    public static class CollectionExtensions
    {
        public static bool IsIn<T>(this T item, params T[] items) =>
            items.Any(x => x.Equals(item));

        public static bool IsNotInt<T>(this T item, params T[] items) =>
            !item.IsIn(items);

        public static bool HasItems<T>(this IEnumerable<T> items) =>
           items != null && items.Any();

        public static bool HasItems<T>(this T[] items) =>
            items != null && items.Any();

        public static bool AreAllElementsUnique<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector) =>
            source.GroupBy(keySelector).All(x => x.Count() == 1);

        public static IEnumerable<TKey> GetDuplicates<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector) =>
            source.GroupBy(keySelector).Where(x => x.Count() > 1).Select(x => x.Key);

        public static bool IsSameAs<TSource>(this IEnumerable<TSource> source, IEnumerable<TSource> collection)
        {
            var firstNotSecond = source.Except(collection);
            var secondNotFirst = collection.Except(source);

            return !firstNotSecond.Any() && !secondNotFirst.Any();
        }

        public static void ForEach<T>(this IEnumerable<T> items, Action<T> action)
        {
            if (items == null)
                return;

            foreach (var item in items)
                action(item);
        }
    }
}
