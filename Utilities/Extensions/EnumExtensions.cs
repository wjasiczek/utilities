﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Utilities.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum value)
        {
            try
            {
                var enumMember = value.GetType().GetMember(value.ToString()).FirstOrDefault();
                var descriptionAttribute = enumMember == null ?
                    default :
                    enumMember.GetCustomAttribute(typeof(DescriptionAttribute)) as DescriptionAttribute;

                return descriptionAttribute == null ?
                    value.ToString() :
                    descriptionAttribute.Description;
            }
            catch
            {
                return value.ToString();
            }
        }

    }
}
